package com.and.phonebook.controllers.Responses;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class WSResponse<T> {

    @JsonProperty
    private final T data;

    public WSResponse(T data) {
        this.data = data;
    }

    public T getData() {
        return data;
    }

}
