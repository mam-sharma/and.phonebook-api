package com.and.phonebook.controllers;

public class ResourcePaths {

    public static final String CONTACT_LIST = "/contacts";
    public static final String ACTIVATE_CONTACT = "/contact/activate/{id}";
    public static final String CUSTOMER_CONTACT_LIST = "/contacts/{id}";

}
