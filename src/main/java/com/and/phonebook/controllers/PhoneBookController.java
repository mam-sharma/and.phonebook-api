package com.and.phonebook.controllers;

import com.and.phonebook.controllers.Responses.WSResponse;
import com.and.phonebook.model.*;
import com.and.phonebook.service.PhoneBookCouchbaseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;


@Api
@RestController
@CrossOrigin(origins = "*")
public class PhoneBookController {


    @Autowired
    private PhoneBookCouchbaseService phoneBookService;


    /**
     * Exposes the URI "/contacts" and "listens" for GET requests. If the request does not contain any parameter,
     * the method will return all contacts that exist in the phonebook.
     * it will return the all contacts.
     *
     * @return a collection of contacts that match with the name, surname and/or phone provided by the request.
     */
    @RequestMapping(method=RequestMethod.GET, path = ResourcePaths.CONTACT_LIST, produces = "application/json")
    @ApiOperation(value = "To get list of all phone numbers")
    public WSResponse<Collection<Contact>> getContacts() {
        Collection<Contact> contacts = phoneBookService.getContacts();
        return new WSResponse<Collection<Contact>>(contacts);
    }


    /**
     * Exposes the URI "/contacts/{id}" and "listens" for GET requests. If the request does not contain any more parameters,
     * the method will return all contacts that exist in the phonebook for a customer,
     * it will return the contacts matching these values.
     *
     * @param customerId - the id of customer.
     * @return a collection of contacts that match with the customerId provided by the request.
     */
    @RequestMapping(method=RequestMethod.GET, path = ResourcePaths.CUSTOMER_CONTACT_LIST, produces = "application/json")
    @ApiOperation(value = "To get list of phone numbers for a customer")
    public WSResponse<Collection<Contact>> getContacts(
            @ApiParam(value = "Customer Id", required=true) @PathVariable String customerId) {
        Collection<Contact> contacts = phoneBookService.getContacts(customerId);
        return new WSResponse<Collection<Contact>>(contacts);
    }


    /**
     * Exposes the URI "/contacts/activate/{id}" and "listens" for PUT reqeusts. If the request does not contain any more parameters,
     * the method will return all contacts that exist in the phonebook. If some of the optional parameters "name", "surname" or "phone" exist in the HTTP request,
     * it will return the contacts matching these values.
     *
     * @param customerId - the customerId.
     * @param phoneNumber - the phone number which needs to be activate.
     * @return a contact which is activated.
     */
    @RequestMapping(method=RequestMethod.PUT, path = ResourcePaths.ACTIVATE_CONTACT, produces = "application/json")
    @ApiOperation(value = "Activate a phone number")
    public WSResponse<Contact> activatePhoneNumber(
            @ApiParam(value = "Customer Id", required=true) @PathVariable String customerId,
            @ApiParam(value = "Phone Number", required=true) @PathVariable String phoneNumber) {
        Contact contact = phoneBookService.activate(customerId, phoneNumber);
        return new WSResponse<Contact>(contact);
    }


}
