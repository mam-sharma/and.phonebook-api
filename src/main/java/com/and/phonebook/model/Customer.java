package com.and.phonebook.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Collection;
import java.util.List;

/**
 * A class containing all the useful information of a customer in the phone book.
 */
@AllArgsConstructor
@Getter
@Setter
public class Customer {
    private String id;
    private String name;
    private String surname;
    private List<Contact> contacts;

}
