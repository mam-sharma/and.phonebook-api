package com.and.phonebook.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * A class containing all the useful information of a contact in the phone book.
 */
@AllArgsConstructor
@Getter
@Setter
public class Contact {

    private String phoneNumber;
    private ContactStatus status;

    public enum ContactStatus {
        ACTIVE,
        INACTIVE

    }

}
