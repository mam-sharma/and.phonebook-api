package com.and.phonebook.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.List;

@Component
@Profile("dev")
@ConfigurationProperties(prefix = "couchbase.dev")
public class DevDatabaseSetting implements DatabaseSetting {

    @NotNull
    private List<String> hosts;
    @NotNull
    private String phoneBookBucket;

    @NotNull
    private String phoneBookBucketPassword;

    public List<String> getHosts() {
        return hosts;
    }

    @Override
    public String getPhoneBookBucket() {
        return phoneBookBucket;
    }

    @Override
    public String getPhoneBookBucketPassword() {
        return phoneBookBucketPassword;
    }

    public void setHosts(List<String> hosts) {
        this.hosts = hosts;
    }

    public void setPhoneBookBucket(String phoneBookBucket) {
        this.phoneBookBucket = phoneBookBucket;
    }

    public void setPhoneBookBucketPassword(String phoneBookBucketPassword) {
        this.phoneBookBucketPassword = phoneBookBucketPassword;
    }

}