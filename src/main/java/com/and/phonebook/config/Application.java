package com.and.phonebook.config;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;
import java.util.TimeZone;


@SpringBootApplication(scanBasePackages = "com.and.phonebook")
@EnableAsync
public class Application {

    private static final TimeZone DEFAULT_TIMEZONE = TimeZone.getTimeZone("GMT");

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }

}
