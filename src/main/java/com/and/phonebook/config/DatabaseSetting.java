package com.and.phonebook.config;

import java.util.List;

public interface DatabaseSetting {
    List<String> getHosts();
    String getPhoneBookBucket();
    String getPhoneBookBucketPassword();
}
