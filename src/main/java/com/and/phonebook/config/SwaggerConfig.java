package com.and.phonebook.config;

import com.google.common.base.Predicate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.AuthorizationScopeBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;
import java.util.List;

import static springfox.documentation.builders.PathSelectors.regex;


@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .enable(true)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.and.phonebook"))
                .paths(internalPaths()).build().pathMapping("/")
                .securityContexts(Collections.singletonList(securityContext()))
                .securitySchemes(Collections.singletonList(apiKey()));
    }


    private SecurityContext securityContext() {
        return SecurityContext.builder()
                .securityReferences(securityReference())
                .forPaths(PathSelectors.any())
                .build();

    }

    private List<SecurityReference> securityReference() {
        AuthorizationScope scope = new AuthorizationScopeBuilder()
                .scope("global")
                .description("everything")
                .build();

        SecurityReference securityReference = new SecurityReference("Authorization", new AuthorizationScope[]{scope});
        return Collections.singletonList(securityReference);
    }


    private ApiKey apiKey() {
        return new ApiKey("Authorization", "Authorization", "header");
    }


    private Predicate<String> internalPaths() {
        return regex("/.*");
    }


    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Phonebook API")
                .description("CRUD Api for Phone Numbers")
                .build();
    }
}