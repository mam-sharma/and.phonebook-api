package com.and.phonebook.config;


import com.couchbase.client.java.Bucket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.couchbase.config.AbstractCouchbaseConfiguration;
import org.springframework.data.couchbase.repository.config.EnableCouchbaseRepositories;

import java.util.List;


@Configuration
@EnableCouchbaseRepositories("com.and.phonebook.repo")
public class DatabaseConfig  extends AbstractCouchbaseConfiguration {

    @Autowired
    private DatabaseSetting dbSetting;

    @Override
    protected String getBucketName() {
        return dbSetting.getPhoneBookBucket();
    }

    @Override
    protected String getBucketPassword() {
        return dbSetting.getPhoneBookBucketPassword();
    }

    @Override
    protected List<String> getBootstrapHosts() {
        return dbSetting.getHosts();
    }

    @Bean
    public Bucket scoresProBucket() throws Exception {
        return couchbaseCluster().openBucket(dbSetting.getPhoneBookBucket(), dbSetting.getPhoneBookBucketPassword());
    }

}