package com.and.phonebook.repo;

import com.and.phonebook.model.Customer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PhoneBookRepository extends CrudRepository<Customer, String> {
}
