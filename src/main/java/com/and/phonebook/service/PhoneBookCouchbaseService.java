package com.and.phonebook.service;

import com.and.phonebook.controllers.exceptions.ContactNotFoundException;
import com.and.phonebook.model.Contact;
import com.and.phonebook.model.Customer;
import com.and.phonebook.repo.PhoneBookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.*;

@Profile("couchbase")
@Service
public class PhoneBookCouchbaseService {

    @Autowired
    private PhoneBookRepository phoneBookRepository;


    public Collection<Contact>  getContacts() {
        Collection<Contact> contacts = new ArrayList<>();
        phoneBookRepository.findAll().forEach(customer -> contacts.addAll(customer.getContacts()));
        return contacts;
    }

    public Collection<Contact>  getContacts(String id) {
        Collection<Contact> contacts = new ArrayList<>();
        phoneBookRepository.findAll().forEach(customer -> {
        if(customer.getId() == id) contacts.addAll(customer.getContacts());
        });

        return contacts;
    }

    public Contact activate(String id, String phoneNumber) {
        Customer customer = phoneBookRepository.findOne(id);
        if (customer != null) {
            ListIterator<Contact> contactItr = customer.getContacts().listIterator();
            while (contactItr.hasNext()) {
                Contact contact = contactItr.next();
                contact.setStatus(Contact.ContactStatus.ACTIVE);
                contactItr.set(contact);
                return contact;
            }
            throw new ContactNotFoundException(phoneNumber);
        } else {
            throw new ContactNotFoundException(id);
        }

    }

}
