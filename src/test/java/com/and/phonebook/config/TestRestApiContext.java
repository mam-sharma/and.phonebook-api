package com.and.phonebook.config;

import org.springframework.context.annotation.*;

/**
 * Rest API test context.
 */
@Configuration
@EnableAspectJAutoProxy
@ComponentScan(basePackages = {"phonebook"})
@PropertySource(value = {"classpath:test.application.properties"})
public class TestRestApiContext {


}
