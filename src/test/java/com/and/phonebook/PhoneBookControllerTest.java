
package com.and.phonebook;

import com.and.phonebook.model.Contact;
import com.and.phonebook.config.TestRestApiContext;
import com.and.phonebook.controllers.Responses.WSResponse;
import com.and.phonebook.controllers.PhoneBookController;
import com.and.phonebook.model.Customer;
import com.and.phonebook.repo.PhoneBookRepository;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.mockito.BDDMockito.given;


@ComponentScan
@ContextConfiguration(classes = {TestRestApiContext.class})
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ActiveProfiles({"test","couchbase"})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Import(PhoneBookController.class)
public class PhoneBookControllerTest {

    @Autowired
    private PhoneBookController controller;

    @MockBean
    private PhoneBookRepository repository;

    private Customer customer;
    private Contact contact;
    private List<Contact> contacts;
    private List<Customer> customers;

    @Before
    public void init() {

        contact = new Contact("9876543210", Contact.ContactStatus.ACTIVE);
        Contact contact2 = new Contact("9876503210", Contact.ContactStatus.ACTIVE);

        contacts = new ArrayList<>();
        contacts.add(contact);
        contacts.add(contact2);
        customer = new Customer("customerId", "Mamta", "Sharma", contacts);


        given(repository.findOne("customerId"))
                .willReturn(customer);

        given(repository.findAll())
                .willReturn(customers);
    }
    @Test
    public void testGetSportsList() {
        WSResponse<Collection<Contact>> response = controller.getContacts();
        Assert.assertEquals("", response.getData());
    }
    @Test
    public void testGetSportsListForCustomer() {
        WSResponse<Collection<Contact>> response = controller.getContacts();
        Assert.assertEquals("", response.getData());
    }
    @Test
    public void testActivatePhoneNumber() {
        WSResponse<Collection<Contact>> response = controller.getContacts();
        Assert.assertEquals("", response.getData());
    }
    @Test
    public void testActivatePhoneNumber_NumberNotExists() {
        WSResponse<Contact> response = controller.activatePhoneNumber("customerId","9876503210");
        Assert.assertEquals("", response.getData());
    }

}

