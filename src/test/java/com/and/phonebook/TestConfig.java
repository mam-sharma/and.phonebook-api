package com.and.phonebook;


import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.PropertySource;

@PropertySource(value = {"classpath:test.application.properties"})
@EnableConfigurationProperties
@SpringBootConfiguration
public class TestConfig {
}